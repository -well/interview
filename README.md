# Instructions

This interview project is an opportunity for you to showcase some of your coding abilities.  
This rails project has obvious problems. ( like it doesn't work :) )  See if you can get it working and do any other refactoring
you see fit.

Try not to go crazy and spend more then a few hours on this.  You don't have to fix everything.

If you have any questions feel free to email me:

melkins@daxko.com

# Steps required to get started

* Clone/Fork the repo
* Run the migrations
* Run the seeder
* Avoid pushing any changes to master