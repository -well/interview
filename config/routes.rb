Rails.application.routes.draw do
  resources :orders
  root      :to => 'home#index', :as => :root
end
