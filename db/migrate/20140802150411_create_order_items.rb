class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.references :order, :index => true
      t.references :item,  :index => true
      t.integer    :quantity, :null => false
      t.decimal    :price,    :null => false
      t.timestamps
    end
  end
end
