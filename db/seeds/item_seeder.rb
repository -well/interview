class ItemSeeder
  def self.seed
    new.seed
  end

  def seed
    unless Item.any?
      p 'creating items'
      create_item 'Red Stapler',       BigDecimal(50)
      create_item 'TPS Report',        BigDecimal(3)
      create_item 'Printer',           BigDecimal(400)
      create_item 'Baseball bat',      BigDecimal(80)
      create_item 'Michael Bolton CD', BigDecimal(12)
    end
  end

private
  def create_item(description, price)
    Item.new.tap do |i|
      i.description = description
      i.price       = price
      p "#{i.description} created" if i.save!
    end
  end
end
